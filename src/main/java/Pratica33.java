import utfpr.ct.dainf.if62c.pratica.Matriz;

/**
 * IF62C Fundamentos de Programação 2
 * Exemplo de programação em Java.
 * @author Wilson Horstmeyer Bogado <wilson@utfpr.edu.br>
 */
public class Pratica33 {

    public static void main(String[] args) {
        
        Matriz matriz1 = new Matriz(2, 2);
        double[][] m = matriz1.getMatriz();
        m[0][0] = 1.0;
        m[0][1] = 2.0;
        m[1][0] = 3.0;
        m[1][1] = 4.0;
        
        Matriz matriz2 = new Matriz(2, 2);
        double[][] p = matriz2.getMatriz();
        p[0][0] = 1.0;
        p[0][1] = 2.0;
        p[1][0] = 3.0;
        p[1][1] = 4.0;
        
        Matriz soma = new Matriz(2,2);
        Matriz produto = new Matriz(2,2);
        
        soma = matriz1.soma(matriz2);
        produto = matriz1.prod(matriz2);
        
        System.out.println(matriz1);
        System.out.println(matriz2);
        
        System.out.println(soma);
        System.out.println(produto);
    }
}
